#Zadanie 2#

Należy pobrać kod i poprawić w nim jak najwięcej błędów.

Klasa **BuggyCode** ma za zadanie wypisać słowa tworzące hasło w pozycji pionowej, lub poziomej. Domyślne hasło to *ATREM*, ale można ustawić własne.

###Przykładowy wynik:###


```
#!java

Automatyka
Telemetria
Regulacja
Elektronika
Metrologia

ATREM
ueele
tlget
oeukr
mmlto
aearl
ttcoo
yrjng
kiaii
aa ka
   a
```


Poprawiony kod należy wrzucić na własne, nowo-utworzone repozytorium, a w tym utworzyć nową gałąź, i dodać do niej plik z numerem przypisanym podczas zgłoszenia.

##UWAGA!##
Nie próbujcie wrzucać poprawionego kodu na to repozytorium!

###Wskazówka###
Do rozwiązania tego zadania nie potrzebna jest znajomość szczegółów implementacyjnych języka Java.