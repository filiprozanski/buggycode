package pl.atrem.praktyki.zadanie.drugie;

import java.util.ArrayList;

public class Starter {
	public static void main(String[] args) {
		BuggyCode code = new BuggyCode();
		code.writeHorizontaly();
		System.out.println();
		System.out.println();
		code.writeVerticaly();
		System.out.println();
		System.out.println();

		ArrayList<String> strings = new ArrayList<String>();
		strings.add("mama");
		strings.add("tata");
		strings.add("siostra");
		strings.add("brat");

		code.setWords(strings);
		code.writeHorizontaly();
		System.out.println();
		System.out.println();
		code.writeVerticaly();
	}
}
