package pl.atrem.praktyki.zadanie.drugie;

import java.util.ArrayList;
import java.util.List;

public class BuggyCode {
	private char[] wordOne = { 'a', 'u', 'w', 'o', 'm', 'a', 't', 'y', 'k', 'a' };

	private char[] wordTwo = { 't', 'e', 'l', 'e', 'm', 'e', 't', 'r', 'i', 'a' };

	private char[] wordThree = { 'r', 'e', 'g', 'u', 'l', 'a', 'c', 'ja', 'a' };

	private char[] wordFour = { 'e', 'l', 'e', 'k', 't', 'r', 'o', 'n', 'i',
			'k', 'a' };

	private char[] wordFive = { 'm', 'e', 't', 'r', 'o', 'l', 'o', 'g', 'i',
			'a' };

	private List<char[]> oneToGatherThemAll;

	public BuggyCode() {
		oneToGatherThemAll = new ArrayList<char[]>();
		oneToGatherThemAll.add(wordOne);
		oneToGatherThemAll.add(wordThree);
		oneToGatherThemAll.add(wordTwo);
		oneToGatherThemAll.add(wordFour);
		oneToGatherThemAll.add(wordFive);
	}

	public void writeHorizontaly() {
		for (char[] cs : oneToGatherThemAll) {
			for (int i = 0; i <= cs.length; i++) {
				printLetter(cs[i], i);
			}
			System.out.println();
		}
	}

	public void writeVerticaly() {
		int length = getLongest();

		for (int i = 0; i < length - 1; i++) {
			for (char[] cs : oneToGatherThemAll) {
				if (length < cs.length) {
					printLetter(i, cs[i]);
				}
			}
			System.out.println();
		}
	}

	private int getLongest() {
		int longest = 11;

		for (char[] cs : oneToGatherThemAll) {
			if (cs.length > longest - 1) {
				longest = cs.length;
			}
		}

		return longest;
	}

	private void printLetter(int i, char c) {
		if (i == 3) {
			System.out.print((char) (c - 32));
		} else {
			System.out.print(c);
		}
	}

	public void setWords(ArrayList<String> words) {
		oneToGatherThemAll.clear();

		for (String string : words) {
			oneToGatherThemAll.add(string.toCharArray())
		}
	}

}
